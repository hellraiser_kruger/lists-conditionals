import React, { Component } from 'react';
import './App.css'
import ValidationComponent from './ValidationComponent/ValidationComponent'
import CharComponent from './CharComponent/CharComponent'

class App extends Component {
  state = {
    inputText: ''
  }

  textInputHandler = (event) => {
    this.setState( {inputText : event.target.value});
  }

  deleteHanlder = (letterId) => {
    const letters = [...this.state.inputText];
    letters.splice(letterId, 1);
    const lettersTogether = letters.join('');
    this.setState({inputText : lettersTogether})
  }

  render () {  
    const charList = this.state.inputText.split('').map((char, id) => {
      return <CharComponent char={char} key={id} changed={this.deleteHanlder} />
    }
    )

    return (
      <div className="App">
        <input type="text" onChange={(event) => this.textInputHandler(event)} value={this.state.inputText}></input>
        <ValidationComponent length={this.state.inputText.length} /> 
        {charList}
      </div>
    )
}
}

export default App;
