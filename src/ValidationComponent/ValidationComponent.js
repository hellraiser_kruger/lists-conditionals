import React from 'react'

const validationComponent = (props) => {
    const minimumLength = 5;
    return (
        props.length > 5 ? "Text too long" : "Text too short"
    )
}
export default validationComponent