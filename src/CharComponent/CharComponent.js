import React from 'react';
import './CharComponent.css'

const charComponent = (props) => {
    return (
        <p className="charcomp" onClick={props.changed}>{props.char}</p>
    )
}

export default charComponent